const IMAGES = [
    'https://gitlab.com/NoahJelen/2048-vanced/-/raw/master/assets/backdrops/andromeda.png',
    'https://gitlab.com/NoahJelen/2048-vanced/-/raw/master/assets/backdrops/earth.png',
    'https://gitlab.com/NoahJelen/2048-vanced/-/raw/master/assets/backdrops/galaxies1.png',
    'https://gitlab.com/NoahJelen/2048-vanced/-/raw/master/assets/backdrops/galaxies2.png',
    'https://gitlab.com/NoahJelen/2048-vanced/-/raw/master/assets/backdrops/galaxy_and_star.png',
    'https://gitlab.com/NoahJelen/2048-vanced/-/raw/master/assets/backdrops/magellanic_cloud.png',
    'https://gitlab.com/NoahJelen/2048-vanced/-/raw/master/assets/backdrops/nebula1.png',
    'https://gitlab.com/NoahJelen/2048-vanced/-/raw/master/assets/backdrops/nebula2.png',
    'https://gitlab.com/NoahJelen/2048-vanced/-/raw/master/assets/backdrops/saturn.png',
    'https://gitlab.com/NoahJelen/2048-vanced/-/raw/master/assets/backdrops/star1.png',
    'https://gitlab.com/NoahJelen/2048-vanced/-/raw/master/assets/backdrops/star2.png',
    'https://gitlab.com/NoahJelen/2048-vanced/-/raw/master/screenshots/game.png',
    'https://gitlab.com/NoahJelen/2048-vanced/-/raw/master/screenshots/loading_screen.png',
    'https://gitlab.com/NoahJelen/2048-vanced/-/raw/master/screenshots/menu.png',
    'https://gitlab.com/NoahJelen/cesium/-/raw/master/screenshots/editor.png',
    'https://www.paradisemod.net/world/underground/cave_badlands.png',
    'https://www.paradisemod.net/world/underground/cave_desert.png',
    'https://www.paradisemod.net/world/underground/cave_icy.png',
    'https://www.paradisemod.net/world/underground/cave_jungle.png',
    'https://www.paradisemod.net/world/underground/cave_normal.png',
    'https://www.paradisemod.net/world/underground/cave_ocean.png',
    'https://www.paradisemod.net/world/underground/cave_rocky_desert.png',
    'https://www.paradisemod.net/world/underground/cave_swamp.png',
    'https://www.paradisemod.net/world/underground/salt_cave.png',
    'https://www.paradisemod.net/world/biomes/christmas_forest.png',
    'https://www.paradisemod.net/world/biomes/autumn_forest.png',
    'https://www.paradisemod.net/world/biomes/concrete_badlands.png',
    'https://www.paradisemod.net/world/biomes/crystal_forest.png',
    'https://www.paradisemod.net/world/biomes/deep_dark_flats.png',
    'https://www.paradisemod.net/world/biomes/polar_winter.png',
    'https://www.paradisemod.net/world/biomes/subglacial_volcanic_field.png',
    'https://www.paradisemod.net/world/biomes/frozen_deep_dark_flats.png',
    'https://www.paradisemod.net/world/biomes/glacier.png',
    'https://www.paradisemod.net/world/biomes/glowing_forest.png',
    'https://www.paradisemod.net/world/biomes/glowing_glacier.png',
    'https://www.paradisemod.net/world/biomes/high_rocky_desert.png',
    'https://www.paradisemod.net/world/biomes/honey_cave.png',
    'https://www.paradisemod.net/world/biomes/mesquite_forest.png',
    'https://www.paradisemod.net/world/biomes/palo_verde_forest.png',
    'https://www.paradisemod.net/world/biomes/rocky_desert.png',
    'https://www.paradisemod.net/world/biomes/rose_field.png',
    'https://www.paradisemod.net/world/biomes/salt_flat.png',
    'https://www.paradisemod.net/world/biomes/silva_insomnium.png',
    'https://www.paradisemod.net/world/biomes/snowy_rocky_desert.png',
    'https://www.paradisemod.net/world/biomes/taiga_insomnium.png',
    'https://www.paradisemod.net/world/biomes/temperate_rainforest.png',
    'https://www.paradisemod.net/world/biomes/the_origin.png',
    'https://www.paradisemod.net/world/biomes/volcanic_field.png',
    'https://www.paradisemod.net/world/biomes/weeping_forest.png',
    'https://www.paradisemod.net/world/dimensions/deep_dark.png',
    'https://www.paradisemod.net/world/dimensions/elysium.png',
    'https://www.paradisemod.net/world/dimensions/overworld_core.png',
    'https://gitlab.com/aercloud-systems/music-lounge/-/raw/master/screenshots/library.png',
    'https://gitlab.com/NoahJelen/the-rock/-/raw/master/screenshots/bookmarks.png',
    'https://gitlab.com/NoahJelen/the-rock/-/raw/master/screenshots/command_line.png',
    'https://gitlab.com/NoahJelen/the-rock/-/raw/master/screenshots/new_bookmark.png',
    'https://gitlab.com/NoahJelen/the-rock/-/raw/master/screenshots/parables.png',
    'https://gitlab.com/NoahJelen/the-rock/-/raw/master/screenshots/tui.png'
];
function genUi() {
    setIcon("/favicon.png");
    setTitle("Aercloud Systems");
    loadStyleSheet("/styles.css");
    loadStyleSheet("/aercloud.css");
    new NavbarBuilder()
        .link("Home","/")
        .link("About Me","/me.html")
        .link("My Projects","/projects")
        .link('<img src="/gitlab.png">My Gitlab',"https://gitlab.com/NoahJelen")
        .build();
    addBanner("/logo.png", IMAGES);
    setBackgroundImage("/background.png");
    addFooter('Copyright hahaha<br/>If you would like to get me a cup of coffee: <a href="https://cash.app/$NoahJelen"><img src="https://cash.app/favicon.ico"></a><a href="https://paypal.me/njelen"><img width="16px" height="16px" src="https://www.paypal.com/favicon.ico"></a>');
}