class NavbarBuilder{
    constructor(){
        this.links=[];
        this.titles=[];
    }
    link(title,link){
        this.links.push(link);
        this.titles.push(title);
        return this;
    }
    text(text){
        this.titles.push(text+" ");
        this.links.push("");
        return this;
    }
    newLine(){
        this.titles.push('\n');
        this.links.push('\n');
        return this;
    }
    build(){
        var navBarText="";
        for(var i=0;i<this.links.length;i++){
            var link = this.links[i];
            var title = this.titles[i];
            if(title=='\n') navBarText+="<p></p>"
            else if(link=="") navBarText+=title;
            else navBarText+=`<a href="${link}">${title}</a>\n`;
        }
        document.documentElement.innerHTML=`<div id="navbar">${navBarText}</div>`+document.documentElement.innerHTML;
    }
}
// set the background image of a webpage
function setBackgroundImage(imagePath){
    var body=document.getElementsByTagName("body")[0];
    body.style["background-image"]=`url('${imagePath}')`;
    body.style["background-repeat"]="no-repeat";
    body.style["background-attachment"]="fixed";
    body.style["background-size"]="cover";
}
// add a banner to the top of the webpage
function addBanner(logoPath,imageList,bannerHeight=11){
    var height=bannerHeight;
    window.onresize=event=>{
        var bannerElem=document.getElementById("banner");
        var logo=document.getElementById("logo");
        if(screen.width<=940){
            logo.style["top"]="2em";
            logo.style["left"]="2em";
            logo.style["height"]="4em";
            bannerElem.style["height"]="7em";
        }
        else {
            logo.style["top"]="2.5em";
            logo.style["left"]="2.5em";
            logo.style["height"]=`${bannerHeight-4}em`;
            bannerElem.style["height"]=`${bannerHeight}em`;
        }
    };
    if(screen.width<=940)height=7;
    var banner=imageList[Math.floor(Math.random()*imageList.length)];
    var bannerTag=
        `<div id="rnd_banner">\
            <img id="banner" src="${banner}" style="height:${height}em;">\
            <img src="${logoPath}" id="logo" style="height:${height-4}em;">\
        </div>`
    document.documentElement.innerHTML=bannerTag + document.documentElement.innerHTML;
}
// add a footer to the webpage
function addFooter(footerText){
    document.documentElement.innerHTML+=`<div id="foot"><p>${footerText}</p></div>`
}
// set the title of the webpage
// also configures the head if it is not yet configured
function setTitle(pageTitle){
    var title=document.getElementsByTagName("title")[0];
    if(title==undefined){
        document.head.innerHTML+=
            `<meta name="viewport" content="width=device-width, initial-scale=1.0">\
            <meta charset="utf-8">\
            <title>${pageTitle}</title>`;
    }
    else title.innerHTML=pageTitle;
}
// load a CSS style sheet
function loadStyleSheet(cssPath){
    document.head.innerHTML+=`<link rel="stylesheet" href="${cssPath}"/>`;
}
// set the icon of a webpage (this is the bookmark icon as well)
function setIcon(iconPath){
    var icon=document.querySelector("link[rel='shortcut icon']");
    if(icon!=undefined)icon.href=iconPath;
    else document.head.innerHTML+=`<link rel="icon" type="image/png" href="${iconPath}"/>`
}